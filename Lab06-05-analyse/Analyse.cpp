#include<fstream>
#include<iostream>
#include<vector>
using namespace std;

class Analyse {
	public:
		int soHost;
		int soInterval;
		vector< vector<long long> > mangInterval;
		vector<long long> soBitTB;
		vector<double> hieuSuat;
		long long kichThuocGoi;
		long long datarate;
		double intervalTime;
		
		void docDuLieu() {
			fstream infile;
			
			infile.open("network.txt", ios::in);
			infile>>soHost;
			infile>>soHost;
			infile.close();
			
			mangInterval.resize(soHost/2);
			
			int thisIndex = 0;
			infile.open("pairsource&des.txt", ios::in);
			for (int i=0; i<soHost; i++) {
				int thisDes;
				infile>>thisDes;
				if (thisDes==-1) {
					fstream infile2;
					char tenFile[1024];
					sprintf(tenFile,"resultforhost%d.txt",i);
					infile2.open(tenFile, ios::in);
					infile2>>soInterval;
					mangInterval[thisIndex].resize(soInterval);
					for (int j=0; j<soInterval; j++) {
						infile2>>mangInterval[thisIndex][j];
					}
					infile2.close();
					thisIndex++;
				}
			}
			infile.close();
			soHost = thisIndex;
			
			cout<<"Nhap kich thuoc goi tin theo b"<<endl;
			cin>>kichThuocGoi;
			cout<<"Nhap datarate duong truyen theo bps "<<endl;
			cin>>datarate;
			cout<<"Nhap intervalTime theo s"<<endl;
			cin>>intervalTime;
			
			soBitTB.resize(soInterval);
			fill(soBitTB.begin(), soBitTB.end(), 0);
			hieuSuat.resize(soInterval);
		}
		
		void tinhKetQua() {
			for (int i= 0; i<soHost; i++) {
				for (int j=0; j<soInterval; j++) {
					mangInterval[i][j] = (long long) (mangInterval[i][j]*kichThuocGoi/intervalTime);
					soBitTB[j] += mangInterval[i][j];
				}
			}
			
			fstream infile;			
			infile.open("throughput.txt", ios::out|ios::trunc);		
			double hieuSuatTB = 0;
			for (int i=0; i<soInterval; i++) {
				soBitTB[i] = (long long) (soBitTB[i]/((double) soHost));
				hieuSuat[i] = (soBitTB[i]/(double) datarate)*100;
				hieuSuatTB+=hieuSuat[i];
				infile<<hieuSuat[i]<<" ";
			}
			infile.close();
			hieuSuatTB/=soInterval;
			cout<<hieuSuatTB;
		}
		
		void thiHanh() {
			docDuLieu();
			tinhKetQua();
		}
};
int main() {
	Analyse a;
	a.thiHanh();
}
