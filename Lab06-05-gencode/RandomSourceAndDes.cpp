#include<fstream>
#include<vector>
#include <stdlib.h>
#include <time.h>  
using namespace std;
class RandomSourceAndDes {
	    public:
		void randomSourceAndDes() {
	    	int soHost = 76;
			vector<int> mangDau;
			mangDau.resize(soHost);
			fill(mangDau.begin(), mangDau.end(), -2);
			srand(time(0));
			int source;
			int des;
			for (int i=0; i<soHost/2; i++) {
				source = rand()%soHost;
				while (mangDau[source]!=-2) {
					source++;
					if (source==soHost) {
						source=0;
					}
				}
				mangDau[source]=source;
				des = rand()%soHost;
				while (mangDau[des]!=-2) {
					des++;
					if (des==soHost) {
						des=0;
					}
				}
				mangDau[source]=des;
				mangDau[des]=-1;		
			}
			ofstream outfile("pairsource&des.txt", ios::out | ios::trunc);
			char dongLenh[1024];
		    for (int i=0; i<soHost; i++) {
				sprintf(dongLenh,"%d ",mangDau[i]);
				outfile<<dongLenh;	    	
		    }	  
			outfile.close();  	
	    }	
};
int main() {
	RandomSourceAndDes r;
	r.randomSourceAndDes();
}
