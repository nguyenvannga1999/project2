#include<stdio.h>
#include<vector>
#include<list>
#include<fstream>
#include <stdlib.h>
#include <time.h>   
#include<queue>
#include<stack>
using namespace std;

class Network {
	public:
		int heSoN;
		int heSoK;
		int soDinh;
		int soHost;		
		vector< list<int> > dsKe;
		vector< int > dsNoiHost;
		
		void docDuLieu() {
			printf("Nhap he so N,he so K va so host\n");
			scanf("%d%d%d",&heSoN,&heSoK,&soHost);
			soDinh = heSoN*heSoN;
		}
		
		void xayDungDsKe() {			
			dsKe.resize(soDinh);
			for (int i=0; i<heSoN; i++) {
				for (int j=0; j<heSoN; j++) {
					if (j<heSoN-1) {
						dsKe[i*heSoN+j].push_back(i*heSoN+j+1);
					}
					if (j>0) {
						dsKe[i*heSoN+j].push_back(i*heSoN+j-1);
					}
					if (i<heSoN-1) {
						dsKe[i*heSoN+j].push_back((i+1)*heSoN+j);
					}
					if (i>0) {
						dsKe[i*heSoN+j].push_back((i-1)*heSoN+j);
					}
				}
			}
			vector<bool> mangDau;
			mangDau.resize(soDinh);
			srand(time(0));
			int nextHop;
			for (int i=0; i<soDinh; i++) {
				fill(mangDau.begin(),mangDau.end(),false);
				mangDau[i] = true;
				list<int>::iterator duyet;
				for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
					nextHop = (int) *duyet;
					mangDau[nextHop] = true;
				}				
				for (int j=0; j<heSoK; j++) {
					nextHop = rand()%soDinh;
					while (mangDau[nextHop]) {
						nextHop++;
						if (nextHop==soDinh) {
							nextHop=0;
						}
					}
					dsKe[i].push_back(nextHop);
					mangDau[nextHop]=true;
				}
			}
		}
		
		void themCacHostOBien() {
			dsNoiHost.resize(soDinh);
			fill(dsNoiHost.begin(), dsNoiHost.end(), -1);
			int i=0, j=0;
			int dem = 0;
			while (dem<soHost && i<heSoN) {
				if (i==0||j==0||i==heSoN-1||j==heSoN-1) {
					dsNoiHost[i*heSoN+j] = dem;
					dem++;
				}
				j++;
				if (j==heSoN) {
					j=0; i++;
				}				
			}
			soHost = dem;			
		}
		
		void taoFileNed() {
			ofstream outfile("network.ned", ios::out | ios::trunc);
			char dongLenh[1024];
			sprintf(dongLenh,"simple Host { \n parameters: \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"@display(\"i=device/cellphone_vs\"); \n double genMsgTime = default(0.0001); \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"int messageLen = default(100000); \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"int bufferSize = default(5); \n double intervalTime = default(0.00001); \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"gates: \n input in; \n output out; \n input forCredit @directIn; \n } \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"simple Switch { \n parameters: \n");
            outfile<<dongLenh;						            
            sprintf(dongLenh,"@display(\"i=device/switch\"); \n int bufferSize = default(5); \n");
            outfile<<dongLenh;	
            sprintf(dongLenh,"double creditDelay = default(0.000000001); \n double switchCycle = default(0.00000001);  \n");
            outfile<<dongLenh;	
            sprintf(dongLenh,"gates: \n input in[]; \n output out[]; \n input forCredit @directIn; \n } \n");
            outfile<<dongLenh;							        
            sprintf(dongLenh,"network Network { \n parameters: \n");
            outfile<<dongLenh;	                     
            sprintf(dongLenh,"int numHost = default(%d); \n int numSwitch = default(%d); \n  double timeSimulation = default(1.0);  \n",soHost,soDinh);
            outfile<<dongLenh;	 
            sprintf(dongLenh,"types: \n channel C extends ned.DatarateChannel { \n delay = 0.1ms; \n datarate=1Gbps; \n } \n");
            outfile<<dongLenh;	 
            sprintf(dongLenh,"submodules: \n host[%d] : Host{ @display(\"p=10,40,m,%d,100,100\"); }; \n switch[%d] : Switch { @display(\"p=100,100,m,%d,100,100\"); }; \n",soHost,soHost,soDinh,heSoN);
            outfile<<dongLenh;	 			            
            sprintf(dongLenh,"connections: \n");
            outfile<<dongLenh;
			for (int i=0; i<soDinh; i++) {
				if (dsNoiHost[i]!=-1) {
					sprintf(dongLenh,"host[%d].in <-- C <-- switch[%d].out++; \n",dsNoiHost[i],i);
	                outfile<<dongLenh;
	                sprintf(dongLenh,"host[%d].out --> C --> switch[%d].in++; \n",dsNoiHost[i],i);
	                outfile<<dongLenh;						
				}
                list<int>::iterator duyet;
                for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
                	int nextHop = (int) *duyet;
                	sprintf(dongLenh,"switch[%d].out++ --> C --> switch[%d].in++; \n",i,nextHop);
                    outfile<<dongLenh;	                  
                }
			}	
            sprintf(dongLenh,"} \n");
            outfile<<dongLenh;
			outfile.close(); 	 			               
		}
		
		void luuDsKe() {
			ofstream outfile("network.txt", ios::out | ios::trunc);
			char dongLenh[1024];
			sprintf(dongLenh,"%d %d\n",soDinh,soHost);
			outfile<<dongLenh;
			for (int i=0; i<soDinh; i++) {
				sprintf(dongLenh,"%d ",dsKe[i].size());
				outfile<<dongLenh;
				list<int>::iterator duyet;
				for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
					int nextHop = (int) *duyet;
					sprintf(dongLenh,"%d ",nextHop);
					outfile<<dongLenh;
				}
				sprintf(dongLenh,"\n");
				outfile<<dongLenh;
				sprintf(dongLenh,"%d\n",dsNoiHost[i]);
				outfile<<dongLenh;
			}
			outfile.close();
		}
		
		void xayDungTopology() {
			xayDungDsKe();
			themCacHostOBien();
			luuDsKe();
			taoFileNed();
		}
		
		void ghiFile(int start, vector<int> duongToiHost) {
			char tenFile[1024];
			fstream outfile;
			sprintf(tenFile,"danhsachkeforswitch%d.txt",start);
			outfile.open(tenFile, ios::out | ios::trunc);
			sprintf(tenFile,"%d ",dsKe[start].size());
			outfile<<tenFile;
			list<int>::iterator duyet;
			for (duyet=dsKe[start].begin(); duyet!=dsKe[start].end(); duyet++) {
				int nextHop = (int) *duyet;
				sprintf(tenFile,"%d ",nextHop);
				outfile<<tenFile;
			}
			sprintf(tenFile,"\n%d",dsNoiHost[start]);
			outfile<<tenFile;			
			outfile.close();
			
			sprintf(tenFile,"routingforswitch%d.txt",start);			
			outfile.open(tenFile, ios::out | ios::trunc);
			for (int i=0; i<duongToiHost.size(); i++) {
				sprintf(tenFile,"%d ",duongToiHost[i]);
				outfile<<tenFile;
			}
			outfile.close();
		}
		
		int getGate(int now, int next) {
			int result = 0;
			list<int>::iterator duyet;
			for (duyet=dsKe[now].begin(); duyet!=dsKe[now].end(); duyet++) {
				int x = (int) *duyet;
				if (x==next) {
					// co lien ket toi host o gate 0
					if (dsNoiHost[now]!=-1) {
						result++;
					}
					return result;
				} else {
					result++;
				}
			}
			return 0;
		}
		
		vector<int> getDuongToiHost(int start, vector<int> before) {
			vector<int> bangRoute;
			bangRoute.resize(before.size());
			fill(bangRoute.begin(), bangRoute.end(), -1);
			stack<int> myStack;
			// tim nextSwitch trong duong di tu Switch start den Switch i bat ky
			for (int i=0; i<bangRoute.size(); i++) {
				if (bangRoute[i]==-1) {
					int j=i;					
				    while (before[j]!=start) {
				    	myStack.push(j);
					    j = before[j];
				    }
				    myStack.push(j);
				    int x;
				    while (!myStack.empty()) {
				    	x = myStack.top();
				    	myStack.pop();
				    	bangRoute[x] = j;
				    }
				}
			}
			// tim gate dan toi tung host
			vector<int> duongToiHost;
			duongToiHost.resize(soHost);
			for (int i=0; i<bangRoute.size(); i++) {
				if (dsNoiHost[i]!=-1) {
					duongToiHost[dsNoiHost[i]] = getGate(start, bangRoute[i]);
				}
			}
			return duongToiHost;
		}
		
		void routing(int start) {
			vector<int> before;
			before.resize(dsKe.size());
			fill(before.begin(), before.end(), -1);
			queue<int> hangDoi;
			before[start] = start;
			hangDoi.push(start);
			int nextHop;
			int add;
			list<int>::iterator duyet;
			while (!hangDoi.empty()) {
				nextHop = hangDoi.front();
				hangDoi.pop();
				for (duyet=dsKe[nextHop].begin(); duyet!=dsKe[nextHop].end(); duyet++) {
					add = (int) *duyet;
					if (before[add]==-1) {
						before[add] = nextHop;
						hangDoi.push(add);
					}
				}
			}
			vector<int> duongToiHost = getDuongToiHost(start,before);
			ghiFile(start, duongToiHost);
		}
	
	    void thucHienRoute() {
	    	for (int i=0; i<dsKe.size(); i++) {
	    		routing(i);
	    	}
	    } 		
	    
	    void randomSourceAndDes() {
			vector<int> mangDau;
			mangDau.resize(soHost);
			fill(mangDau.begin(), mangDau.end(), -2);
			srand(time(0));
			int source;
			int des;
			for (int i=0; i<soHost/2; i++) {
				source = rand()%soHost;
				while (mangDau[source]!=-2) {
					source++;
					if (source==soHost) {
						source=0;
					}
				}
				mangDau[source]=source;
				des = rand()%soHost;
				while (mangDau[des]!=-2) {
					des++;
					if (des==soHost) {
						des=0;
					}
				}
				mangDau[source]=des;
				mangDau[des]=-1;		
			}
			ofstream outfile("pairsource&des.txt", ios::out | ios::trunc);
			char dongLenh[1024];
		    for (int i=0; i<soHost; i++) {
				sprintf(dongLenh,"%d ",mangDau[i]);
				outfile<<dongLenh;	    	
		    }	  
			outfile.close();  	
	    }
};
int main() {
	Network n;
	n.docDuLieu();
	n.xayDungTopology();
	n.thucHienRoute();
	n.randomSourceAndDes();
}



