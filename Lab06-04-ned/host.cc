#include<omnetpp.h>
#include<message_m.h>
using namespace std;
class Host : public cSimpleModule {
protected:
    int soHost;
    cMessage* genClock;
    virtual void initialize();
    virtual Message* genNewMessage();
    virtual void forwardMessage(Message* newMsg);
    virtual void handleMessage(cMessage* msg);
};
Define_Module(Host);
void Host::initialize() {
    soHost = (int) getParentModule()->par("numHost");
    genClock = new cMessage("genClock");
    scheduleAt(simTime()+0.005, genClock);
    Message* newMsg = genNewMessage();
    forwardMessage(newMsg);
}
Message* Host::genNewMessage() {
    Message* newMsg = new Message();
    newMsg->setSource(getIndex());
    int des = intuniform(0, soHost-1);
    if (des==getIndex()) {
        des++;
        if (des==soHost) {
            des=0;
        }
    }
    newMsg->setDestination(des);
    return newMsg;
}
void Host::forwardMessage(Message* newMsg) {
    send(newMsg,"out");
}
void Host::handleMessage(cMessage* msg) {
    if (msg==genClock) {
        Message* newMsg = genNewMessage();
        forwardMessage(newMsg);
        scheduleAt(simTime()+0.005, genClock);
    } else {
        bubble("Nhan duoc thu");
        delete msg;
    }
}



