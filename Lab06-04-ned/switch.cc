#include<omnetpp.h>
#include<vector>
#include<message_m.h>
#include<fstream>
using namespace std;
class Switch : public cSimpleModule {
protected:
    int soHost;
    vector<int> mangNextHop;
    vector<int> mangGate;
    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual int getGateIndex(bool isHost, int nextHop);
    virtual void createMangGate();
};

Define_Module(Switch);

void Switch::initialize() {
    soHost = (int) getParentModule()->par("numHost");
    mangNextHop.resize(soHost);
    char tenfile[1024];
    sprintf(tenfile,"routingforswitch%d.txt",getIndex());
    fstream infile;
    infile.open(tenfile, ios::in);
    for (int i=0; i<soHost; i++) {
        infile>>mangNextHop[i];
    }
    infile.close();
    createMangGate();
}

void Switch::createMangGate() {
    char tenfile[1024];
    sprintf(tenfile,"danhsachkeforswitch%d.txt",getIndex());
    fstream infile;
    infile.open(tenfile, ios::in);
    int bac;
    infile>>bac;
    mangGate.resize(bac+1);
    mangGate[0] = -1;
    for (int i=1; i<=bac; i++) {
        infile>>mangGate[i];
    }
    infile.close();
}

void Switch::handleMessage(cMessage* msg) {
    if (Message* newMsg = dynamic_cast<Message*> (msg)) {
        if (newMsg->getDestination()==getIndex()) {
            int gateIndex = getGateIndex(true,getIndex());
            send(msg,"out",gateIndex);
        } else {
            int gateIndex = getGateIndex(false, mangNextHop[newMsg->getDestination()]);
            send(msg,"out",gateIndex);
        }
    }
}

int Switch::getGateIndex(bool isHost, int nextHop) {
    if (isHost) {
        for (int i=0; i<mangGate.size(); i++) {
            if (mangGate[i]==-1) {
                return i;
            }
        }
    } else {
        for (int i=0; i<mangGate.size(); i++) {
            if (mangGate[i]==nextHop) {
                return i;
            }
        }
    }
}




