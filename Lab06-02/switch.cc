#include<omnetpp.h>
#include<message_m.h>
#include<receivedmsg_m.h>
#include<creditmsg_m.h>
#include<list>
#include<vector>
#include<limits.h>
using namespace std;
class Switch : public cSimpleModule {
protected:
    unsigned int numSource;
    vector< list<Message*> > entranceBuffer;
    vector< list<Message*> > exitBuffer;
    vector< list<Message*> >middleBuffer;
    unsigned int bufferSize;
    double creditDelay;
    double switchCycle;
    bool connectFree;
    cMessage* sendClock;
    cMessage* middleClock;

    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void addMessage(Message* msg);
    virtual void checkEntranceBuffer();
    virtual void sendMessage();
};
Define_Module(Switch);

void Switch::initialize() {
    numSource = (unsigned int) getParentModule()->par("numSource");
    //for all source and one destination
    entranceBuffer.resize(numSource+1);
    exitBuffer.resize(numSource+1);
    middleBuffer.resize(numSource+1);

    bufferSize = (int) par("bufferSize");
    creditDelay = (double) par("creditDelay");
    switchCycle = (double) par("switchCycle");

    sendClock = new cMessage("sendClock");
    middleClock = new cMessage("middleClock");
    connectFree = true;
}

void Switch::handleMessage(cMessage* msg) {
    if (msg==sendClock) {
        if (!exitBuffer[numSource].empty() && connectFree) {
            send(exitBuffer[numSource].front(),"gate$o",numSource);
            exitBuffer[numSource].pop_front();
            connectFree = false;
            checkEntranceBuffer();
        }
    } else {
        if (msg==middleClock) {
            if (!middleBuffer.empty() && exitBuffer[numSource].size()<bufferSize) {
                exitBuffer[numSource].push_back(middleBuffer[numSource].front());
                middleBuffer[numSource].pop_front();
                sendMessage();
                checkEntranceBuffer();
            }
        } else {
            if (Message* thisMsg = dynamic_cast<Message*> (msg)) {
                //send received flag message
                ReceivedMsg* flagMsg = new ReceivedMsg();
                cModule* switchModule = getParentModule()->getSubmodule("source",thisMsg->getSource());
                simtime_t propagation = 0;
                simtime_t duration = 0;
                sendDirect(flagMsg,propagation,duration, switchModule,"forCredit");
                addMessage(thisMsg);
            } else {
                // is received flag message
                if (ReceivedMsg* receivedMsg = dynamic_cast<ReceivedMsg*> (msg)) {
                    delete receivedMsg;
                    connectFree = true;
                    sendMessage();
                    checkEntranceBuffer();
                }
            }
        }
    }
}

void Switch::addMessage(Message* msg) {
    entranceBuffer[msg->getSource()].push_back(msg);
    checkEntranceBuffer();
}

void Switch::checkEntranceBuffer() {
    int minID=INT_MAX;
    int bufferID;
    int thisID;
    while (middleBuffer[numSource].size()+exitBuffer[numSource].size()<bufferSize) {
        bufferID=-1;
        for (int i=0; i<numSource; i++) {
            if (!entranceBuffer[i].empty()) {
                thisID = entranceBuffer[i].front()->getId();
                if (thisID<minID) {
                    minID=thisID;
                    bufferID=i;
                }
            }
        }
        if (bufferID==-1) {
            sendMessage();
            break;
        } else {
            middleBuffer[numSource].push_back(entranceBuffer[bufferID].front());
            entranceBuffer[bufferID].pop_front();
            if (middleClock->isScheduled()) {
            } else {
                scheduleAt(simTime()+switchCycle, middleClock);
            }
            CreditMsg* creditMsg = new CreditMsg();
            creditMsg->setCount(bufferSize-entranceBuffer[bufferID].size());
            cModule* switchModule = getParentModule()->getSubmodule("source",bufferID);
            simtime_t propagation = creditDelay;
            simtime_t duration = 0;
            sendDirect(creditMsg,propagation,duration, switchModule,"forCredit");
            sendMessage();
        }
    }
    sendMessage();
}

void Switch::sendMessage() {
    if (!exitBuffer[numSource].empty() && connectFree) {
        if (sendClock->isScheduled()) {
        } else {
            scheduleAt(simTime()+switchCycle, sendClock);
        }
    }
}







