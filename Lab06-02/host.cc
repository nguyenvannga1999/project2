#include<list>
#include<message_m.h>
#include<omnetpp.h>
#include<receivedmsg_m.h>
#include<creditmsg_m.h>
using namespace std;

class Host : public cSimpleModule {
protected:
    //ExitBuffer Size
    unsigned int bufferSize;

    //Next Switch's EntranceBuffer Remain Size
    int count;

    double timeSimulation;

    int thisGenMsgTime;
    int numGenMsgTime;
    int numMsgGen;
    double genMsgTime;

    int thisIntervalTime;
    int numIntervalTime;
    double intervalTime;


    int countReceive;

    list<Message*> sourceQueue;
    list<Message*> exitBuffer;
    list<int> countEachInterval;
    cMessage* intervalClock;
    cMessage* genMsgClock;

    bool connectFree;


    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void checkExitBuffer();
    virtual void generateMessage();
    virtual void addMessage(Message* msg);
    virtual void finish();
};

Define_Module(Host);

void Host::initialize() {
    bufferSize = (unsigned int) par("bufferSize");

    count = (int) getParentModule()->getSubmodule("switch")->par("bufferSize");

    timeSimulation = (double) getParentModule()->par("timeSimulation");

    thisGenMsgTime = 0;
    genMsgTime = (double) par("genMsgTime");
    numGenMsgTime = (int) (timeSimulation/genMsgTime);
    numMsgGen = 0;

    thisIntervalTime = 0;
    intervalTime = par("intervalTime");
    numIntervalTime = (int) (timeSimulation/intervalTime);

    countReceive = 0;
    connectFree = true;

    if (strcmp("destination",getName())==0) {
        intervalClock = new cMessage("intervalClock");
        scheduleAt(simTime()+intervalTime, intervalClock);
    }
    if (strcmp("source",getName())==0) {
        genMsgClock = new cMessage("genMsgClock");
        generateMessage();
    }
}

void Host::generateMessage() {
    Message* newMsg = new Message();
    newMsg->setId(numMsgGen);
    newMsg->setSource(getIndex());
    numMsgGen++;
    scheduleAt(simTime()+genMsgTime, genMsgClock);
    addMessage(newMsg);
}

void Host::addMessage(Message* msg) {
    sourceQueue.push_back(msg);
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        exitBuffer.push_back(sourceQueue.front());
        sourceQueue.pop_front();
    }
    checkExitBuffer();
}

void Host::checkExitBuffer() {
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        exitBuffer.push_back(sourceQueue.front());
        sourceQueue.pop_front();
    }
    if (!exitBuffer.empty() && count>0) {
        if (connectFree) {
            send(exitBuffer.front(),"gate$o");
            exitBuffer.pop_front();
            count--;
            connectFree = false;
        }
    }
}

void Host::handleMessage(cMessage* msg) {
    if (msg==genMsgClock) {
        thisGenMsgTime++;
        if (thisGenMsgTime<=numGenMsgTime) {
            generateMessage();
        }
    } else {
        if (msg==intervalClock) {
            countEachInterval.push_back(countReceive);
            countReceive=0;
            thisIntervalTime++;
            if (thisIntervalTime<=numIntervalTime) {
                scheduleAt(simTime()+intervalTime, intervalClock);
            }
        } else {
            // is message from source host
            if (Message* thisMsg = dynamic_cast<Message*> (msg)) {
                delete thisMsg;
                countReceive++;
                ReceivedMsg* flagMsg = new ReceivedMsg();
                cModule* switchModule = getParentModule()->getSubmodule("switch");
                simtime_t propagation = 0;
                simtime_t duration = 0;
                sendDirect(flagMsg,propagation,duration, switchModule,"forCredit");
            } else {
                // is received flag message
                if (ReceivedMsg* receivedMsg = dynamic_cast<ReceivedMsg*> (msg)) {
                    connectFree = true;
                    delete receivedMsg;
                    checkExitBuffer();
                } else {
                    //is Credit_interval message
                    if (CreditMsg* creditMsg = dynamic_cast<CreditMsg*> (msg)) {
                        count = creditMsg->getCount();
                        delete creditMsg;
                        checkExitBuffer();
                    }
                }
            }
        }
    }
}

void Host::finish() {
    if (strcmp("destination",getName())==0) {
        int dem =0;
        while (!countEachInterval.empty()) {
            dem++;
            EV<<"\nIntervalTime "<<dem<<" nhan duoc "<<countEachInterval.front()<<" goi tin";
            countEachInterval.pop_front();
        }
    }
}



