#include<stdio.h>
#include<vector>
#include<list>
#include<fstream>
#include <stdlib.h>
#include <time.h>   
using namespace std;
class Network {
	public:
		int heSoN;
		int heSoK;
		int heSoHost;
		vector< list<int> > dsKe;
		vector< int > dsNoiHost;
		
		void docDuLieu() {
			printf("Nhap he so N,he so K va so host\n");
			scanf("%d%d%d",&heSoN,&heSoK,&heSoHost);
		}
		
		void xayDungDsKe() {
			int soDinh = heSoN*heSoN;
			dsKe.resize(soDinh);
			for (int i=0; i<heSoN; i++) {
				for (int j=0; j<heSoN; j++) {
					if (j<heSoN-1) {
						dsKe[i*heSoN+j].push_back(i*heSoN+j+1);
					}
					if (j>0) {
						dsKe[i*heSoN+j].push_back(i*heSoN+j-1);
					}
					if (i<heSoN-1) {
						dsKe[i*heSoN+j].push_back((i+1)*heSoN+j);
					}
					if (i>0) {
						dsKe[i*heSoN+j].push_back((i-1)*heSoN+j);
					}
				}
			}
			vector<bool> mangDau;
			mangDau.resize(soDinh);
			srand(time(0));
			int nextHop;
			for (int i=0; i<soDinh; i++) {
				fill(mangDau.begin(),mangDau.end(),false);
				mangDau[i] = true;
				list<int>::iterator duyet;
				for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
					nextHop = (int) *duyet;
					mangDau[nextHop] = true;
				}				
				for (int j=0; j<heSoK; j++) {
					nextHop = rand()%soDinh;
					while (mangDau[nextHop]) {
						nextHop++;
						if (nextHop==soDinh) {
							nextHop=0;
						}
					}
					dsKe[i].push_back(nextHop);
					mangDau[nextHop]=true;
				}
			}
		}
		
		void themCacHostOBien() {
			dsNoiHost.resize(heSoN*heSoN);
			fill(dsNoiHost.begin(), dsNoiHost.end(), -1);
			int i=0, j=0;
			int dem = 0;
			while (dem<heSoHost && i<heSoN) {
				if (i==0||j==0||i==heSoN-1||j==heSoN-1) {
					dsNoiHost[i*heSoN+j] = dem;
					dem++;
				}
				j++;
				if (j==heSoN) {
					j=0; i++;
				}				
			}
			heSoHost = dem;			
		}
		
		void taoFileNed() {
			int soDinh = heSoN*heSoN;
			ofstream outfile("network.ned", ios::out | ios::trunc);
			char dongLenh[1024];
			sprintf(dongLenh,"simple Host { \n parameters: \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"@display(\"i=device/cellphone_vs\"); \n double genMsgTime = default(0.005); \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"int bufferSize = default(2); \n double intervalTime = default(0.005); \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"gates: \n input in; \n output out; \n input forCredit @directIn; \n } \n");
            outfile<<dongLenh;
            sprintf(dongLenh,"simple Switch { \n parameters: \n");
            outfile<<dongLenh;						            
            sprintf(dongLenh,"@display(\"i=device/switch\"); \n int bufferSize = default(2); \n");
            outfile<<dongLenh;	
            sprintf(dongLenh,"double creditDelay = default(0.001); \n double switchCycle = default(0.005);  \n");
            outfile<<dongLenh;	
            sprintf(dongLenh,"gates: \n input in[]; \n output out[]; \n input forCredit @directIn; \n } \n");
            outfile<<dongLenh;							        
            sprintf(dongLenh,"network Network { \n parameters: \n");
            outfile<<dongLenh;	                     
            sprintf(dongLenh,"int numHost = default(%d); \n int numSwitch = default(%d); \n  double timeSimulation = default(1.0);  \n",heSoHost,soDinh);
            outfile<<dongLenh;	 
            sprintf(dongLenh,"types: \n channel C extends ned.DelayChannel { \n delay = 15ms; \n } \n");
            outfile<<dongLenh;	 
            sprintf(dongLenh,"submodules: \n host[%d] : Host{ @display(\"p=10,40,m,%d,100,100\"); }; \n switch[%d] : Switch { @display(\"p=100,100,m,%d,100,100\"); }; \n",heSoHost,heSoHost,soDinh,heSoN);
            outfile<<dongLenh;	 			            
            sprintf(dongLenh,"connections: \n");
            outfile<<dongLenh;
			for (int i=0; i<soDinh; i++) {
				if (dsNoiHost[i]!=-1) {
					sprintf(dongLenh,"host[%d].in <-- C <-- switch[%d].out++; \n",dsNoiHost[i],i);
	                outfile<<dongLenh;
	                sprintf(dongLenh,"host[%d].out --> C --> switch[%d].in++; \n",dsNoiHost[i],i);
	                outfile<<dongLenh;						
				}
                list<int>::iterator duyet;
                for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
                	int nextHop = (int) *duyet;
                	sprintf(dongLenh,"switch[%d].out++ --> C --> switch[%d].in++; \n",i,nextHop);
                    outfile<<dongLenh;	                  
                }
			}	
            sprintf(dongLenh,"} \n");
            outfile<<dongLenh;
			outfile.close(); 	 			               
		}
		
		void luuDsKe() {
			int soDinh = heSoN*heSoN;
			ofstream outfile("network.txt", ios::out | ios::trunc);
			char dongLenh[1024];
			sprintf(dongLenh,"%d %d\n",soDinh,heSoHost);
			outfile<<dongLenh;
			for (int i=0; i<soDinh; i++) {
				sprintf(dongLenh,"%d ",dsKe[i].size());
				outfile<<dongLenh;
				list<int>::iterator duyet;
				for (duyet=dsKe[i].begin(); duyet!=dsKe[i].end(); duyet++) {
					int nextHop = (int) *duyet;
					sprintf(dongLenh,"%d ",nextHop);
					outfile<<dongLenh;
				}
				sprintf(dongLenh,"\n");
				outfile<<dongLenh;
				sprintf(dongLenh,"%d\n",dsNoiHost[i]);
				outfile<<dongLenh;
			}
			outfile.close();
		}
};
int main() {
	Network n;
	n.docDuLieu();
	n.xayDungDsKe();
	n.themCacHostOBien();
	n.luuDsKe();
	n.taoFileNed();
}
