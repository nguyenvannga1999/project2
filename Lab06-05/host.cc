#include<list>
#include<message_m.h>
#include<omnetpp.h>
#include<receivedmsg_m.h>
#include<creditmsg_m.h>
#include<fstream>
using namespace std;

class Host : public cSimpleModule {
protected:
    //ExitBuffer Size
    unsigned int bufferSize;

    //Next Switch's EntranceBuffer Remain Size
    int count;

    double timeSimulation;

    int thisGenMsgTime;
    int numGenMsgTime;
    int numMsgGen;
    double genMsgTime;

    int thisIntervalTime;
    int numIntervalTime;
    double intervalTime;


    int countReceive;
    int sumReceive;
    int numHost;
    int myDestination;

    list<Message*> sourceQueue;
    list<Message*> exitBuffer;
    list<int> countEachInterval;
    cMessage* intervalClock;
    cMessage* genMsgClock;

    bool connectFree;
    int sumSend;


    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void checkExitBuffer();
    virtual void generateMessage();
    virtual void addMessage(Message* msg);
    virtual void finish();
};

Define_Module(Host);

void Host::initialize() {
    fstream infile;
    infile.open("pairsource&des.txt", ios::in);
    for (int i=0; i<=getIndex(); i++) {
        infile>>myDestination;
    }
    infile.close();

    bufferSize = (unsigned int) par("bufferSize");
    count = (int) getParentModule()->getSubmodule("switch",0)->par("bufferSize");

    timeSimulation = (double) getParentModule()->par("timeSimulation");

    thisGenMsgTime = 0;
    genMsgTime = (double) par("genMsgTime");
    numGenMsgTime = (int) (timeSimulation/genMsgTime);
    numMsgGen = 0;

    thisIntervalTime = 0;
    intervalTime = par("intervalTime");
    numIntervalTime = (int) (timeSimulation/intervalTime);
    sumReceive = 0;

    numHost = getParentModule()->par("numHost");

    countReceive = 0;
    connectFree = true;
    sumSend = 0;

    if (myDestination==-1) {
        intervalClock = new cMessage("intervalClock");
        scheduleAt(simTime()+intervalTime, intervalClock);
    } else {
        genMsgClock = new cMessage("genMsgClock");
        generateMessage();
    }
}

void Host::generateMessage() {
    Message* newMsg = new Message("message");
    newMsg->setSource(getIndex());
    newMsg->setDestination(myDestination);
    newMsg->setBitLength((int) par("messageLen"));
    numMsgGen++;
    scheduleAt(simTime()+genMsgTime, genMsgClock);
    addMessage(newMsg);
}

// push message to sorceQueue
// check if can push to exitBuffer then move to exitBuffer
void Host::addMessage(Message* msg) {
    sourceQueue.push_back(msg);
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        exitBuffer.push_back(sourceQueue.front());
        sourceQueue.pop_front();
    }
    checkExitBuffer();
}

// check if can send message
void Host::checkExitBuffer() {
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        exitBuffer.push_back(sourceQueue.front());
        sourceQueue.pop_front();
    }
    if (!exitBuffer.empty() && count>0) {
        if (connectFree) {
            sumSend++;
            send(exitBuffer.front(),"out");
            exitBuffer.pop_front();
            count--;
            connectFree = false;
        }
    }
}

void Host::handleMessage(cMessage* msg) {
    // if genMsgClock then create new message
    if (msg==genMsgClock) {
        thisGenMsgTime++;
        if (thisGenMsgTime<=numGenMsgTime) {
            generateMessage();
        }
    } else {
        // if intervalClock then save amount of received message in this interval time
        if (msg==intervalClock) {
            countEachInterval.push_back(countReceive);
            countReceive=0;
            thisIntervalTime++;
            if (thisIntervalTime<=numIntervalTime) {
                scheduleAt(simTime()+intervalTime, intervalClock);
            }
        } else {
            // if is message from another host then send message (ACK) to tell pre-node that message has just arrived
            if (Message* thisMsg = dynamic_cast<Message*> (msg)) {
                countReceive++;
                ReceivedMsg* flagMsg = new ReceivedMsg("ACK message");
                flagMsg->setDestination(thisMsg->getDestination());
                cModule* switchModule = thisMsg->getSenderModule();
                simtime_t propagation = 0;
                simtime_t duration = 0;
                sendDirect(flagMsg,propagation,duration, switchModule,"forCredit");

                CreditMsg* creditMsg = new CreditMsg("Credit message");
                creditMsg->setDestination(thisMsg->getDestination());
                creditMsg->setCount(bufferSize);
                sendDirect(creditMsg,propagation,duration, switchModule,"forCredit");

                delete thisMsg;
            } else {
                // is received-flag message (ACK message) then set the connection is free
                if (ReceivedMsg* receivedMsg = dynamic_cast<ReceivedMsg*> (msg)) {
                    connectFree = true;
                    delete receivedMsg;
                    checkExitBuffer();
                } else {
                    //is Credit_interval message then set the count (available position on next-node)
                    if (CreditMsg* creditMsg = dynamic_cast<CreditMsg*> (msg)) {
                        count = creditMsg->getCount();
                        delete creditMsg;
                        checkExitBuffer();
                    }
                }
            }
        }
    }
}

void Host::finish() {
    EV<<"\n\n\nHost "<<getIndex();
    if (myDestination==-1) {
        char tenFile[1024];
        fstream outfile;
        sprintf(tenFile,"resultforhost%d.txt",getIndex());
        outfile.open(tenFile, ios::out | ios::trunc);
        sprintf(tenFile,"%d ",countEachInterval.size());
        outfile<<tenFile;
        EV<<"\nSo Interval "<<countEachInterval.size();
        while (!countEachInterval.empty()) {
            sumReceive+=countEachInterval.front();
            sprintf(tenFile,"%d ",countEachInterval.front());
            outfile<<tenFile;
            countEachInterval.pop_front();
        }
        EV<<"\nDo dai IntervalTime "<<intervalTime<<"second";
        EV<<"\nTong goi tin nhan duoc "<<sumReceive;
        sumReceive = (int) (sumReceive/timeSimulation);
        EV<<"\nSo goi tin nhan duoc trong 1second "<<sumReceive;
        outfile.close();
    } else {
        EV<<"\nTong so goi tin gui di "<<sumSend;
    }
}





