#include<omnetpp.h>
#include<message_m.h>
#include<receivedmsg_m.h>
#include<creditmsg_m.h>
#include<list>
#include<vector>
#include<algorithm>
#include<limits.h>
#include<fstream>
#include<movemsg_m.h>
#include<queue>

using namespace std;
class Switch : public cSimpleModule {
protected:
    unsigned int numHost;
    unsigned int numGateOut;
    unsigned int numGateIn;
    double timeSimulation;

    vector< list<Message*> > entranceBuffer;
    vector<int> mangNextGate;
    vector< list<Message*> > exitBuffer;
    vector<int> nowExitBufferSize;
    vector<double> timeMove;

    unsigned int bufferSize;
    double creditDelay;
    double switchCycle;

    vector<bool> connectFree;
    vector<cMessage*> sendClock;
    vector<int> counter;

    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void addMessage(Message* msg);
    virtual void checkEntranceBuffer();
    virtual void sendMessage(int index);
};
Define_Module(Switch);

void Switch::initialize() {
    timeSimulation = (double) getParentModule()->par("timeSimulation");
    numHost = (unsigned int) getParentModule()->par("numHost");
    numGateOut = gateSize("out");
    numGateIn = gateSize("in");

    //read routing data from file
    // the index is ID of destination host
    // the value is ID of gate in this switch that we can forward message to destination
    mangNextGate.resize(numHost);
    char tenfile[1024];
    sprintf(tenfile,"routingforswitch%d.txt",getIndex());
    fstream infile;
    infile.open(tenfile, ios::in);
    for (int i=0; i<numHost; i++) {
        infile>>mangNextGate[i];
    }
    infile.close();

    //for all gate, each will have entranceBuffer, exitBuffer, middleBuffer
    entranceBuffer.resize(numGateIn);
    exitBuffer.resize(numGateOut);
    nowExitBufferSize.resize(numGateOut);
    fill(nowExitBufferSize.begin(), nowExitBufferSize.end(), 0);

    bufferSize = (int) par("bufferSize");
    creditDelay = (double) par("creditDelay");
    switchCycle = (double) par("switchCycle");

    //each gate will have sendClock, middleClock, connectFree bool variable and counter variable
    sendClock.resize(numGateOut);
    for (int i=0; i<numGateOut; i++) {
        sendClock[i] = new cMessage("sendClock");
    }

    timeMove.resize(numGateIn);
    fill(timeMove.begin(), timeMove.end(), -1.0);

    connectFree.resize(numGateOut);
    fill(connectFree.begin(), connectFree.end(), true);

    counter.resize(numGateOut);
    fill(counter.begin(), counter.end(), bufferSize);
}

void Switch::handleMessage(cMessage* msg) {
    // if is  sendClock that forward message to next node
    if (find(sendClock.begin(), sendClock.end(), msg)!=sendClock.end()) {
        // get the index of this gate
        int index = find(sendClock.begin(), sendClock.end(), msg)-sendClock.begin();
        if (!exitBuffer[index].empty() && connectFree[index] && counter[index]>0) {
            nowExitBufferSize[index]--;
            send(exitBuffer[index].front(),"out",index);
            exitBuffer[index].pop_front();
            counter[index]--;
            connectFree[index] = false;
            //check if can send another message through this gate
            sendMessage(index);
            checkEntranceBuffer();
        }
    } else {
        // if is move message to move from entrance to exit
        if (MoveMsg* moveMsg = dynamic_cast<MoveMsg*> (msg)) {
            for (int i=0; i<numGateIn; i++) {
                if (timeMove[i]==moveMsg->getTime()) {
                    timeMove[i]=-1;
                    Message* thisMsg = entranceBuffer[i].front();
                    entranceBuffer[i].pop_front();
                    exitBuffer[mangNextGate[thisMsg->getDestination()]].push_back(thisMsg);

                    // send credit message to let pre-node know available position on this gate
                    CreditMsg* creditMsg = new CreditMsg("credit message");
                    creditMsg->setCount(bufferSize-entranceBuffer[i].size());
                    creditMsg->setDestination(thisMsg->getDestination());
                    cModule* preModule = thisMsg->getSenderModule();
                    simtime_t propagation = creditDelay;
                    simtime_t duration = 0;
                    sendDirect(creditMsg,propagation,duration, preModule,"forCredit");

                    sendMessage(mangNextGate[thisMsg->getDestination()]);
                }
            }
        } else {
            // is message from source host to destination host
            if (Message* thisMsg = dynamic_cast<Message*> (msg)) {
                //send received flag message (ACK)
                ReceivedMsg* flagMsg = new ReceivedMsg("ACK message");
                flagMsg->setDestination(thisMsg->getDestination());
                cModule* preModule = thisMsg->getSenderModule();
                simtime_t propagation = 0;
                simtime_t duration = 0;
                sendDirect(flagMsg,propagation,duration, preModule,"forCredit");

                // push to entrance queue
                addMessage(thisMsg);
            } else {
                // is received flag message (ACK)
                if (ReceivedMsg* receivedMsg = dynamic_cast<ReceivedMsg*> (msg)) {
                    // get index of gate that the message was sent
                    int index = mangNextGate[receivedMsg->getDestination()];
                    connectFree[index] = true;
                    delete receivedMsg;
                    sendMessage(index);
                    checkEntranceBuffer();
                } else {
                    // is CreditMessage then update count variable for the gate
                    CreditMsg* creditMsg = (CreditMsg*) msg;
                    counter[mangNextGate[creditMsg->getDestination()]] = creditMsg->getCount();
                    sendMessage(mangNextGate[creditMsg->getDestination()]);
                    delete creditMsg;
                    checkEntranceBuffer();
                }
            }
        }
    }
}

void Switch::addMessage(Message* msg) {
    entranceBuffer[msg->getArrivalGate()->getIndex()].push_back(msg);
    checkEntranceBuffer();
}

void Switch::checkEntranceBuffer() {
    if (simTime()<=timeSimulation) {
        vector< vector<int> > bufferID;
        bufferID.resize(numGateOut);
        for (int i=0; i<numGateOut; i++) {
            bufferID[i].resize(bufferSize);
        }

        vector<int> bufferIDSize;
        bufferIDSize.resize(numGateOut);
        fill(bufferIDSize.begin(), bufferIDSize.end(), 0);

        for (int i=0; i<numGateIn; i++) {
            if (!entranceBuffer[i].empty() && timeMove[i]==-1.0) {
                int desGate = mangNextGate[entranceBuffer[i].front()->getDestination()];
                if (nowExitBufferSize[desGate]<bufferSize) {
                    bufferID[desGate][bufferIDSize[desGate]] = i;
                    bufferIDSize[desGate]++;
                    nowExitBufferSize[desGate]++;
                } else {
                    if (bufferIDSize[desGate]>0) {
                        int maxIndex = 0;
                        for (int j=1; j<bufferIDSize[desGate]; j++) {
                            if (entranceBuffer[bufferID[desGate][j]].front()->getId() > entranceBuffer[bufferID[desGate][maxIndex]].front()->getId()) {
                                maxIndex = j;
                            }
                        }
                        if (entranceBuffer[bufferID[desGate][maxIndex]].front()->getId() > entranceBuffer[i].front()->getId()) {
                            bufferID[desGate][maxIndex] = i;
                        }
                    }
                }
            }
        }

        double nowTime = simTime().dbl();

        for (int i=0; i<numGateOut; i++) {
            for (int j=0; j<bufferIDSize[i]; j++) {
                timeMove[bufferID[i][j]] = nowTime;
            }
        }

        MoveMsg* moveMsg = new MoveMsg();
        moveMsg->setTime(nowTime);
        scheduleAt(simTime()+creditDelay,moveMsg);
    }
}

void Switch::sendMessage(int index) {
    if (!exitBuffer[index].empty() && connectFree[index] && counter[index]>0) {
        if (sendClock[index]->isScheduled()) {
        } else {
            scheduleAt(simTime()+switchCycle, sendClock[index]);
        }
    }
}











