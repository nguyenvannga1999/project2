#include<omnetpp.h>
#include<vector>
#include<message_m.h>
#include<fstream>
using namespace std;
class Switch : public cSimpleModule {
protected:
    int soHost;
    vector<int> mangNextGate;
    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
};

Define_Module(Switch);

void Switch::initialize() {
    soHost = (int) getParentModule()->par("numHost");
    mangNextGate.resize(soHost);
    char tenfile[1024];
    sprintf(tenfile,"routingforswitch%d.txt",getIndex());
    fstream infile;
    infile.open(tenfile, ios::in);
    for (int i=0; i<soHost; i++) {
        infile>>mangNextGate[i];
    }
    infile.close();
}

void Switch::handleMessage(cMessage* msg) {
    if (Message* newMsg = dynamic_cast<Message*> (msg)) {
        int gateIndex = mangNextGate[newMsg->getDestination()];
        newMsg->setHopCount(newMsg->getHopCount()+1);
        send(msg,"out",gateIndex);
    }
}



