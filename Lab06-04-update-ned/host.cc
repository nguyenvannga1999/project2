#include<omnetpp.h>
#include<message_m.h>
using namespace std;
class Host : public cSimpleModule {
protected:
    int soHost;
    int count;
    cMessage* genClock;
    virtual void initialize();
    virtual Message* genNewMessage();
    virtual void handleMessage(cMessage* msg);
};
Define_Module(Host);
void Host::initialize() {
    soHost = (int) getParentModule()->par("numHost");
    genClock = new cMessage("genClock");
    count = 1;
    scheduleAt(simTime()+0.005, genClock);
    Message* newMsg = genNewMessage();
    send(newMsg,"out");
}
Message* Host::genNewMessage() {
    Message* newMsg = new Message();
    newMsg->setSource(getIndex());
    int des = intuniform(0, soHost-1);
    if (des==getIndex()) {
        des++;
        if (des==soHost) {
            des=0;
        }
    }
    newMsg->setDestination(des);
    return newMsg;
}
void Host::handleMessage(cMessage* msg) {
    if (msg==genClock) {
        Message* newMsg = genNewMessage();
        send(newMsg,"out");
        count++;
        if (count<5) {
            scheduleAt(simTime()+0.005, genClock);
        }
    } else {
        Message* thisMsg = (Message*) msg;
        if (thisMsg->getDestination()!=getIndex()) {
            EV<<"False destination! this message is not for me";
        } else {
            EV<<"From "<<thisMsg->getSource()<<" to "<<thisMsg->getDestination()<<" hopcount "<<thisMsg->getHopCount();
        }
        delete msg;
    }
}



