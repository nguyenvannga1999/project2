#include<stdio.h>
#include<omnetpp.h>
#include<list>
using namespace std;
class Nguon : public cSimpleModule {
protected:
    double chuKySinh;
    unsigned int bufferSize;
    list<cMessage*> sourceQueue;
    list<cMessage*> exitBuffer;
    double timeGiaLap;
    cMessage* clock;
    cMessage* wakeup;
    int count;
    simtime_t lastSend;

    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void forwardMessage(cMessage* msg);
    virtual void checkBuffer();
};

Define_Module(Nguon);

void Nguon::initialize() {
    chuKySinh = (double) par("chuKySinh");
    bufferSize = (unsigned int) par("bufferSize");
    timeGiaLap = (double) getParentModule()->par("timeGiaLap");
    count = (int) (timeGiaLap/chuKySinh);
    clock = new cMessage("clock");
    scheduleAt(simTime()+chuKySinh, clock);
    wakeup = new cMessage("wakeup");
    scheduleAt(simTime()+0.00001, wakeup);
    lastSend = simTime()-timeGiaLap;
    cMessage* newMsg = new cMessage("new message");
    forwardMessage(newMsg);
}

void Nguon::handleMessage(cMessage* msg){
    if (msg==clock) {
        cMessage* newMsg = new cMessage("new message");
        forwardMessage(newMsg);
        count--;
        if (count>0) {
            scheduleAt(simTime()+chuKySinh, clock);
        }
    } else {
        if (msg==wakeup) {
            checkBuffer();
            if (count>0) {
                scheduleAt(simTime()+0.00001, wakeup);
            }
        }
    }
}

void Nguon::forwardMessage(cMessage* msg){
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        cMessage* addMsg = sourceQueue.front();
        sourceQueue.pop_front();
        exitBuffer.push_back(addMsg);
    }
    if (exitBuffer.size()<bufferSize) {
        exitBuffer.push_back(msg);
    } else {
        sourceQueue.push_back(msg);
    }
    cGate* myGate = gate("gate$o");
    cChannel* myChannel = myGate->getChannel();
    cDelayChannel* myDelay = (cDelayChannel*) myChannel;
    simtime_t delay = myDelay->getDelay();
    if (simTime()-delay>lastSend) {
        lastSend = simTime();
        send(exitBuffer.front(),"gate$o");
        exitBuffer.pop_front();
    }

}

void Nguon::checkBuffer() {
    while (!sourceQueue.empty() && exitBuffer.size()<bufferSize) {
        cMessage* addMsg = sourceQueue.front();
        sourceQueue.pop_front();
        exitBuffer.push_back(addMsg);
    }
    if (!exitBuffer.empty()) {
        cGate* myGate = gate("gate$o");
        cChannel* myChannel = myGate->getChannel();
        cDelayChannel* myDelay = (cDelayChannel*) myChannel;
        simtime_t delay = myDelay->getDelay();
        if (simTime()-delay>lastSend) {
            lastSend = simTime();
            send(exitBuffer.front(),"gate$o");
            exitBuffer.pop_front();
        }
    }
}




