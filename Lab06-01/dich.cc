#include<stdio.h>
#include<omnetpp.h>
#include<list>
using namespace std;
class Dich : public cSimpleModule {
protected:
    float intervalTime;
    list<int> ketQua;
    float timeGiaLap;
    cMessage* clock;
    int thisTime;
    int count;

    virtual void initialize();
    virtual void handleMessage(cMessage* msg);
    virtual void forwardMessage(cMessage* msg);
    virtual void finish();
};

Define_Module(Dich);

void Dich::initialize() {
    intervalTime = (double) par("intervalTime");
    timeGiaLap = (double) getParentModule()->par("timeGiaLap");
    clock = new cMessage("clock");
    scheduleAt(simTime()+intervalTime, clock);
    thisTime = 0;
    count = (int) (timeGiaLap/intervalTime);
}

void Dich::handleMessage(cMessage* msg){
    if (msg==clock) {
        ketQua.push_back(thisTime);
        thisTime = 0;
        count--;
        if (count>0) {
            scheduleAt(simTime()+intervalTime, clock);
        }
    } else {
        thisTime++;
        delete msg;
    }
}

void Dich::forwardMessage(cMessage* msg){
}

void Dich::finish() {
    int dem =0;
    while (!ketQua.empty()) {
        dem++;
        EV<<"\nIntervalTime "<<dem<<" nhan duoc "<<ketQua.front()<<" goi tin";
        ketQua.pop_front();
    }
}


