#include<stdio.h>
#include<fstream>
#include<vector>
#include<list>
#include<queue>
#include<stack>
using namespace std;

class Routing {
	public:
		int soDinh;
		int soHost;
		vector< list<int> > dsKe;
		vector<int> dsNoiHost;
		
		void docDuLieu() {
			fstream infile;
			infile.open("network.txt", ios::in);
			infile>>soDinh>>soHost;
			dsKe.resize(soDinh);
			dsNoiHost.resize(soDinh);
			for (int i=0; i<soDinh; i++) {
				int bac;
				infile>>bac;
				int nextHop;
				for (int j=0; j<bac; j++) {
					infile>>nextHop;
					dsKe[i].push_back(nextHop);
				}
				infile>>dsNoiHost[i];
			}
		}
		
		void ghiFile(int start, vector<int> duongToiHost) {
			char tenFile[1024];
			fstream outfile;
			sprintf(tenFile,"danhsachkeforswitch%d.txt",start);
			outfile.open(tenFile, ios::out | ios::trunc);
			sprintf(tenFile,"%d ",dsKe[start].size());
			outfile<<tenFile;
			list<int>::iterator duyet;
			for (duyet=dsKe[start].begin(); duyet!=dsKe[start].end(); duyet++) {
				int nextHop = (int) *duyet;
				sprintf(tenFile,"%d ",nextHop);
				outfile<<tenFile;
			}
			sprintf(tenFile,"\n%d",dsNoiHost[start]);
			outfile<<tenFile;			
			outfile.close();
			
			sprintf(tenFile,"routingforswitch%d.txt",start);			
			outfile.open(tenFile, ios::out | ios::trunc);
			for (int i=0; i<duongToiHost.size(); i++) {
				sprintf(tenFile,"%d ",duongToiHost[i]);
				outfile<<tenFile;
			}
			outfile.close();
		}
		
		int getGate(int now, int next) {
			int result = 0;
			list<int>::iterator duyet;
			for (duyet=dsKe[now].begin(); duyet!=dsKe[now].end(); duyet++) {
				int x = (int) *duyet;
				if (x==next) {
					// co lien ket toi host o gate 0
					if (dsNoiHost[now]!=-1) {
						result++;
					}
					return result;
				} else {
					result++;
				}
			}
			return 0;
		}
		
		vector<int> getDuongToiHost(int start, vector<int> before) {
			vector<int> bangRoute;
			bangRoute.resize(before.size());
			fill(bangRoute.begin(), bangRoute.end(), -1);
			stack<int> myStack;
			// tim nextSwitch trong duong di tu Switch start den Switch i bat ky
			for (int i=0; i<bangRoute.size(); i++) {
				if (bangRoute[i]==-1) {
					int j=i;					
				    while (before[j]!=start) {
				    	myStack.push(j);
					    j = before[j];
				    }
				    myStack.push(j);
				    int x;
				    while (!myStack.empty()) {
				    	x = myStack.top();
				    	myStack.pop();
				    	bangRoute[x] = j;
				    }
				}
			}
			// tim gate dan toi tung host
			vector<int> duongToiHost;
			duongToiHost.resize(soHost);
			for (int i=0; i<bangRoute.size(); i++) {
				if (dsNoiHost[i]!=-1) {
					duongToiHost[dsNoiHost[i]] = getGate(start, bangRoute[i]);
				}
			}
			return duongToiHost;
		}
		
		void routing(int start) {
			vector<int> before;
			before.resize(dsKe.size());
			fill(before.begin(), before.end(), -1);
			queue<int> hangDoi;
			before[start] = start;
			hangDoi.push(start);
			int nextHop;
			int add;
			list<int>::iterator duyet;
			while (!hangDoi.empty()) {
				nextHop = hangDoi.front();
				hangDoi.pop();
				for (duyet=dsKe[nextHop].begin(); duyet!=dsKe[nextHop].end(); duyet++) {
					add = (int) *duyet;
					if (before[add]==-1) {
						before[add] = nextHop;
						hangDoi.push(add);
					}
				}
			}
			vector<int> duongToiHost = getDuongToiHost(start,before);
			ghiFile(start, duongToiHost);
		}
	
	    void thucHienRoute() {
	    	for (int i=0; i<dsKe.size(); i++) {
	    		routing(i);
	    	}
	    } 
};

int main() {
	Routing r;
	r.docDuLieu();
	r.thucHienRoute();
}
